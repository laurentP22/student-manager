<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Student extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        /* 
        load only the : 
        student_id, last_name, middle_name, first_name, gender
        (doesn't load the created_at and update_at)
        */

        return [
            'student_id' => $this->student_id,
            'last_name' => $this->last_name,
            'middle_name' => $this->middle_name,
            'first_name' => $this->first_name,
            'gender' => $this->gender
        ];
        

    }
}

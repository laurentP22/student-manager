<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            // create the columns, see https://laravel.com/docs/5.7/migrations
            $table->increments('student_id'); // Auto-incrementing UNSIGNED INTEGER (primary key) equivalent column.
            $table->string('last_name', 45);  // VARCHAR
            $table->string('middle_name', 45);// VARCHAR
            $table->string('first_name', 45); // VARCHAR
            $table->enum('gender', ['male', 'female']); // ENUM
            $table->boolean('display')->default(true); //BOOLEAN
            $table->timestamps();   // DATETIME (created_at and updated_at)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}

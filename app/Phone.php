<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $primaryKey = 'phone_id';

    public function student()
    {
        return $this->belongsTo('App\Student','student_id');
    }
}

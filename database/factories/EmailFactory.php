<?php

use Faker\Generator as Faker;

$factory->define(App\Email::class, function (Faker $faker) {
    return [
        //'student_id' => random_int(\DB::table('students')->min('student_id'), \DB::table('students')->max('student_id')),
        'email' => $faker->unique()->email,
        'email_type'=> $faker->randomElement(['professional', 'personal'])
    ];
});

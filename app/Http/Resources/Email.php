<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Email extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        /* 
        load only the : 
        email, student_id, email_type
        (doesn't load the created_at and update_at)
        */
        return [
            'email' => $this->email,
            'student_id' => $this->student_id,
            'email_type' => $this->email_type
        ];
    }
}

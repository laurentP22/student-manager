<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Email;
use App\Http\Resources\Email as EmailResource;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource for a specified student.
     *
     * @return \Illuminate\Http\Response
    */
    public function indexForStudent($student_id)
    {
        $emails = Email::where([
            ['student_id', $student_id],
            ['display',true]])
            ->orderBy('created_at','asc')
            ->get();
        // Return collection of students as a resource
        // use collection to return a list
        return EmailResource::collection($emails);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emails = Email::where('display',true)->orderBy('created_at','asc')->get();
        // Return collection of students as a resource
        // use collection to return a list
        return EmailResource::collection($emails);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if put request we take the Id of the email to then edit it 
        // else we create new email

        if($request->isMethod('put') ){
            $email_id = $request->input('email');
            $email = Email::findOrFail($email_id);
            $email->student_id = $request->input('student_id');
            $email->email_type = $request->input('email_type');
            $email->display = $request->input('display');
        }else{
            $email = new Email;
            $email->email = $request->input('email');
            $email->student_id = $request->input('student_id');
            $email->email_type = $request->input('email_type');
        }

        // save the phone and return it
        if($email->save()) {
            return new EmailResource($email);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email_id)
    {
        $email = Email::where('display',true)->findOrFail($email_id);
        //Return the phone as a resource
        return new EmailResource($email);
    }

    /**
     * Hide the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hide($email_id)
    {
        // Get a single article
        $email = Email::findOrFail($email_id);
        $email->display = 0;

        ;
        //Return the article as a resource
        if($email->save()) {
            return new EmailResource($email);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($email)
    {
        $email = Email::findOrFail($email);
        //Check if there is more than 1 email
        $studentId = $email->student_id;
        $nbEmail = Email::where('student_id', $studentId)->count() ;
        if($nbEmail <= 1){
            return "You can't delete this email, you have to keep at least one email";
        }

        
        if($email->delete()) {
            return new EmailResource($email);
        }    
    }
}

<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeder
        factory(App\Student::class, 20)->create()->each(
            function($student) {
                factory(App\Address::class)->create(['student_id' => $student->student_id]);
                factory(App\Email::class)->create(['student_id' => $student->student_id]);
                factory(App\Phone::class)->create(['student_id' => $student->student_id]);
            }
        );
    }
    
}

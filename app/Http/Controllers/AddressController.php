<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Address;
use App\Http\Resources\Address as AddressResource;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource for a specified student.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexForStudent($student_id)
    {
        $addresses = Address::where([
            ['student_id', $student_id],
            ['display',true]])
            ->orderBy('address_id','asc')
            ->get();
        // Return collection of students as a resource
        // use collection to return a list
        return AddressResource::collection($addresses);
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Address::where('display',true)->orderBy('address_id','asc')->get();
        // Return collection of students as a resource
        // use collection to return a list
        return AddressResource::collection($addresses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if put request we take the Id of the address to then edit it 
        // else we create new address

        if($request->isMethod('put') ){
            $address_id = $request->input('address_id');
            $address = Address::findOrFail($address_id);
            $address->student_id = $request->input('student_id');
            $address->address_line = $request->input('address_line');
            $address->city = $request->input('city');
            $address->zip_postcode = $request->input('zip_postcode');
            $address->state = $request->input('state');
            $address->display = $request->input('display');
        }else{
            $address = new Address;
            $address->address_id = $request->input('address_id');
            $address->student_id = $request->input('student_id');
            $address->address_line = $request->input('address_line');
            $address->city = $request->input('city');
            $address->zip_postcode = $request->input('zip_postcode');
            $address->state = $request->input('state');
        }

        // save the address and return it
        if($address->save()) {
            return new AddressResource($address);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($address_id)
    {
        $address = Address::where('display',true)->findOrFail($address_id);
        //Return the address as a resource
        return new AddressResource($address);
    }
    /**
     * Hide the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function hide($address_id)
    {
        // Get a single address
        $address = Address::findOrFail($address_id);
        $address->display = 0;

        ;
        //Return the address as a resource
        if($address->save()) {
            return new AddressResource($address);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($address_id)
    {
        // Get address
        $address = Address::findOrFail($address_id);
        // And delete it 
        $studentId = $address->student_id;
        $nbAddress = Address::where('student_id', $studentId)->count() ;
        if($nbAddress <= 1){
            return "You can't delete this email, you have to keep at least one email";
        }        
        if($address->delete()) {
            return new AddressResource($address);
        }    
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Phone extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        /* 
        load only the : 
        phone_id, student_id, phone, phone_type,
        country_code, area_code
        (doesn't load the created_at and update_at)
        */
        if($this->phone_id){
            return [
                'phone_id' => $this->phone_id,
                'student_id' => $this->student_id,
                'phone' => $this->phone,
                'phone_type' => $this->phone_type,
                'country_code' => $this->country_code,
                'area_code' => $this->area_code
            ];
        }else{
            return [
                'phone' => $this->phone
            ];
        }

    }
}

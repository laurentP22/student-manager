<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        // phone_id build in the seeder
        //'student_id' => random_int(\DB::table('students')->min('student_id'), \DB::table('students')->max('student_id')),
        'address_line' => $faker->streetAddress,
        'city' => $faker->city,
        'zip_postcode' => $faker->postcode,
        'state' => $faker->state
    ];
});

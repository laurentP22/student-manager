<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Students Manager</title>
        <!-- Protection against cross site injuries -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>window.laravel = { csrfTOken: '{{ csrf_token() }}' }</script>
        
        <!-- Fonts 
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        -->
        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <app-navbar></app-navbar>
            <br>
            <div class="container">
                <router-view></router-view>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
    <script>
    </script>
</html>

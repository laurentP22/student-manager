<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('address_id'); // Auto-incrementing UNSIGNED INTEGER (primary key) equivalent column.
            $table->integer('student_id')->unsigned()->nullable(); // UNSIGNED INT and set it nullable
            $table->foreign('student_id')->references('student_id')->on('students')->onDelete('set null')->onUpdate('cascade');//Foreign Key
            $table->string('address_line', 100);    // VARCHAR
            $table->string('city', 45);             // VARCHAR
            $table->string('zip_postcode', 45);        // VARCHAR
            $table->string('state', 45);  // VARCHAR
            $table->boolean('display')->default(true); //BOOLEAN
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}

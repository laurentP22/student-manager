<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'last_name' => $faker->lastName,
        'gender'=> $gender,
        'middle_name'=> $faker->firstName($gender),
        'first_name'=> $faker->firstName($gender)
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Phone::class, function (Faker $faker) {
    return [
        // phone_id build in the seeder
        //'student_id' => random_int(\DB::table('students')->min('student_id'), \DB::table('students')->max('student_id')),
        'phone' => $faker->phoneNumber,
        'phone_type' => $faker->randomElement(['mobile', 'landline']),
        'country_code' => '+58',
        'area_code' =>$faker->randomElement(['668', '554','350'])
    ];
});

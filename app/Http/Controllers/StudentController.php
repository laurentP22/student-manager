<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Student;
use App\Address;
use App\Email;
use App\Phone;

use App\Http\Resources\Student as StudentResource;
use App\Http\Resources\Address as AddressResource;
use App\Http\Resources\Email as EmailResource;
use App\Http\Resources\Phone as PhoneResource;
use App\Http\Resources\StudentWithPhones as StudentWithPhonesResource;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::orderBy('student_id','desc')->paginate(5);
        // Return collection of students as a resource
        // use collection to return a list
        return StudentResource::collection($students);
    }
    /**
     * Display a listing of the resource for a specified student.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexStudentWithPhones()
    {
        $students = Student::where('display',true)->orderBy('student_id','desc')->paginate(5);

        foreach ($students as $student) {
            $phones = Phone::where([
                ['student_id', $student->student_id],
                ['display',true]])
                ->select('phone')
                ->get();
            $student->phones = $phones;
        }
        
        // Return collection of students as a resource
        // use collection to return a list


        return StudentWithPhonesResource::collection($students);
        
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if put request we take the Id of the phone to then edit it 
        // else we create new phone

        if($request->isMethod('put') ){
            $student_id = $request->input('student_id');
            $student = Student::findOrFail($student_id);
            $student->last_name = $request->input('last_name');
            $student->middle_name = $request->input('middle_name');
            $student->first_name = $request->input('first_name');
            $student->gender = $request->input('gender');

            if($student->save()) {
                return new StudentResource($student);
            }
        }else{
            // Check if the email is available
            $email_id = $request->input('email.email');
            $emailExists = Email::where('email', $email_id)->exists();
            if($emailExists){
                return 'Error, the email is already used.';
            }
            
            // Create a new student
            $student = new Student;
            $student->student_id = $request->input('student.student_id');
            $student->last_name = $request->input('student.last_name');
            $student->middle_name = $request->input('student.middle_name');
            $student->first_name = $request->input('student.first_name');
            $student->gender = $request->input('student.gender');

            // Create a new address
            $address = new Address;
            $address->address_id = $request->input('address.address_id');
            $address->address_line = $request->input('address.address_line');
            $address->city = $request->input('address.city');
            $address->zip_postcode = $request->input('address.zip_postcode');
            $address->state = $request->input('address.state');

            // Create a new phone
            $phone = new Phone;
            $phone->phone_id = $request->input('phone.phone_id');
            $phone->phone = $request->input('phone.phone');
            $phone->phone_type = $request->input('phone.phone_type');
            $phone->country_code = $request->input('phone.country_code');
            $phone->area_code = $request->input('phone.area_code');

            // Create a new email
            $email = new Email;
            $email->email = $request->input('email.email');
            $email->email_type = $request->input('email.email_type');

            // Save the record to the database
            $student->save();
            $student->emails()->save($email);
            $student->addresses()->save($address);
            $student->phones()->save($phone);

            // Return a Json of the new items
            return [
                new StudentResource($student),
                new PhoneResource($phone),
                new EmailResource($email),
                new AddressResource($address)
            ];
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($student_id)
    {
        // Get a single article
        $student = Student::where('display',true)->findOrFail($student_id);
        //Return the article as a resource
        return new StudentResource($student);
    }
    /**
     * Hide the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
     public function hide($student_id)
    {
        // Get a single article
        $student = Student::findOrFail($student_id);
        $student->display = 0;

        $addresses = Address::where('student_id',$student->student_id)->update(array('display' => 0));
        $emails = Email::where('student_id',$student->student_id)->update(array('display' => 0));
        $phones = Phone::where('student_id',$student->student_id)->update(array('display' => 0));
        ;
        //Return the article as a resource
        if($student->save()) {
            return new StudentResource($student);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($student_id)
    {
        // Get student and then delete it
        $student = Student::findOrFail($student_id);
        if($student->delete()) {
            return new StudentResource($student);
        }    
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->increments('phone_id'); // Auto-incrementing UNSIGNED INTEGER (primary key) equivalent column.
            $table->integer('student_id')->unsigned(); // UNSIGNED INT
            $table->foreign('student_id')->references('student_id')->on('students')->onDelete('cascade')->onUpdate('cascade');//Foreign Key
            $table->string('phone', 30);             // VARCHAR
            $table->enum('phone_type', ['mobile', 'landline']); // ENUM
            $table->string('country_code', 5);             // VARCHAR
            $table->string('area_code', 5);             // VARCHAR
            $table->boolean('display')->default(true); //BOOLEAN
            $table->timestamps(); // DATETIME (created_at and updated_at)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phones');
    }
}

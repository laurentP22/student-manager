<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $primaryKey = 'email';
    public $incrementing = false;
    
    public function student()
    {
        return $this->belongsTo('App\Student','student_id');
    }
}

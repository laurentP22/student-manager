<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Phone;
use App\Http\Resources\Phone as PhoneResource;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource for a specified student.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexForStudent($student_id)
    {
        $phones = Phone::where([
            ['student_id', $student_id],
            ['display',true]])
            ->orderBy('created_at','asc')
            ->get();
        // Return collection of students as a resource
        // use collection to return a list
        return PhoneResource::collection($phones);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phones = Phone::where('display',true)->orderBy('created_at','asc')->get();
        // Return collection of students as a resource
        // use collection to return a list
        return PhoneResource::collection($phones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // if put request we take the Id of the phone to then edit it 
        // else we create new phone

        if($request->isMethod('put') ){
            $phone_id = $request->input('phone_id');
            $phone = Phone::findOrFail($phone_id);
            $phone->student_id = $request->input('student_id');
            $phone->phone = $request->input('phone');
            $phone->phone_type = $request->input('phone_type');
            $phone->country_code = $request->input('country_code');
            $phone->area_code = $request->input('area_code');
            $phone->display = $request->input('display');

        }else{
            $phone = new Phone;
            $phone->phone_id = $request->input('phone_id');
            $phone->student_id = $request->input('student_id');
            $phone->phone = $request->input('phone');
            $phone->phone_type = $request->input('phone_type');
            $phone->country_code = $request->input('country_code');
            $phone->area_code = $request->input('area_code');
        }

        // save the phone and return it
        if($phone->save()) {
            return new PhoneResource($phone);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($phone_id)
    {
        $phone = Phone::findOrFail($phone_id);
        //Return the phone as a resource
        return new PhoneResource($phone);
    }
    /**
     * Hide the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hide($phone_id)
    {
        // Get a single article
        $phone = Phone::findOrFail($phone_id);
        $phone->display = 0;

        ;
        //Return the article as a resource
        if($phone->save()) {
            return new PhoneResource($phone);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($phone_id)
    {
        // Get phone
        $phone = Phone::findOrFail($phone_id);

        //Check if there is more than 1 phone
        $studentId = $phone->student_id;
        $nbPhone = Phone::where('student_id', $studentId)->count() ;
        if($nbPhone <= 1){
            return "You can't delete this phone, you have to keep at least one phone";
        }
        // Delete the phone
        if($phone->delete()) {
            return new PhoneResource($phone);
        }    
    }
}
